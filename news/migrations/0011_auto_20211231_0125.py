# Generated by Django 3.2.9 on 2021-12-30 22:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0010_post_available'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={},
        ),
        migrations.AlterField(
            model_name='category',
            name='slug',
            field=models.SlugField(max_length=100),
        ),
        migrations.AlterField(
            model_name='category',
            name='title',
            field=models.CharField(max_length=250),
        ),
        migrations.AlterIndexTogether(
            name='post',
            index_together=set(),
        ),
        migrations.RemoveField(
            model_name='post',
            name='available',
        ),
    ]
