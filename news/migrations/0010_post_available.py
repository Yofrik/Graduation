# Generated by Django 3.2.9 on 2021-12-30 21:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0009_alter_post_index_together'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='available',
            field=models.BooleanField(default=True),
        ),
    ]
