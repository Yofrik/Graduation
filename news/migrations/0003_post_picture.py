# Generated by Django 3.2.9 on 2021-12-24 14:28

from django.db import migrations
import easy_thumbnails.fields


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0002_auto_20211222_2003'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='picture',
            field=easy_thumbnails.fields.ThumbnailerImageField(blank=True, upload_to='photos'),
        ),
    ]
