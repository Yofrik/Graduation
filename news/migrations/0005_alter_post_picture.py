# Generated by Django 3.2.9 on 2021-12-24 14:47

from django.db import migrations
import easy_thumbnails.fields


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0004_alter_post_picture'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='picture',
            field=easy_thumbnails.fields.ThumbnailerImageField(default='photos/noimg.png', upload_to='photos'),
        ),
    ]
