from django.urls import path
from .views import home_view, detail_view, tagged, news_add, category_news
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('news/', home_view, name="home"),
    path('news/<slug>/', category_news, name="category_news"),
    path('news/add/', news_add, name="news_add"),
    path('news/post/<slug:slug>/', detail_view, name="detail"),
    path('news/tag/<slug:slug>/', tagged, name="tagged"),
]
