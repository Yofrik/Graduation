from django.db import models
from taggit.managers import TaggableManager
from easy_thumbnails.fields import ThumbnailerImageField
from django.urls import reverse


class Category(models.Model):
    title = models.CharField(max_length=250)
    picture = models.ImageField(upload_to='content', default='photos/noimg.png')
    slug = models.SlugField(max_length=100)
    def __str__(self):
        return self.title

class Post(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True)
    title = models.CharField(max_length=250)
    description = models.TextField()
    published = models.DateField(auto_now_add=True)
    slug = models.SlugField(unique=True, max_length=100)
    tags = TaggableManager()
    picture = ThumbnailerImageField(upload_to='photos', default='photos/noimg.png')

    def __str__(self):
        return self.title

