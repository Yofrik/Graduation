from django import forms

from .models import Order


class OrderCreateForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ['first_name', 'last_name', 'email', 'address', 'postal_code', 'city']
        widgets = {
            'email': forms.EmailInput(attrs={"class": "form-control", "placeholder": "e-mail"}),
            'postal_code': forms.TextInput(attrs={"class": "form-control", "placeholder": "Почтовый индекс"}),
            'city': forms.TextInput(attrs={"class": "form-control", "placeholder": "Город"}),
            'address': forms.TextInput(attrs={"class": "form-control", "placeholder": "Улица"}),
        }
