from cart.cart import Cart
from django.shortcuts import render, redirect
from .forms import OrderCreateForm
from .models import OrderItem
from .tasks import order_created
from django.urls import reverse
from .models import Order
from django.contrib.auth.models import User


# Create your views here.


def order_create(request):
    cart = Cart(request)
    if request.method == 'POST':
        form = OrderCreateForm(request.POST)
        if form.is_valid():
            order = form.save(commit=False)
            order.user = request.user  # Получаешь авторизованного пользователя
            order.save()  # Сохраняешь значения из формы
            order = form.save()
            for item in cart:
                OrderItem.objects.create(order=order, product=item['product'],
                                         price=item['price'],
                                         quantity=item['quantity'], user=request.user)

            cart.clear()
            order_created.delay(order.id)
            # set the order in the session
            request.session['order_id'] = order.id
            # redirect for payment
            return redirect(reverse('payment:process'))
    else:
        form = OrderCreateForm()
    return render(request, 'order_create.html',
                  {'cart': cart, 'form': form})


def history(request):
    product = OrderItem.objects.filter(user=request.user)
    return render(request, 'history.html',
                  {'product': product})
