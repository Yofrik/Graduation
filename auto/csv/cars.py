import csv

with open('auto/csv/cars_csv.csv', 'r') as cars:
    cars_csv_file = csv.reader(cars)
    brands = []
    models = []
    for line in cars_csv_file:
        strings = line[0]
        line2 = strings.split(';')
        line3 = line2[0]
        line4 = line2[1]
        if line3 not in brands:
            brands.append(line3)
        if line4 not in models:
            models.append(line4)
    brand_choice = tuple(zip(brands, brands))
    models_choice = tuple(zip(models, models))
