import django_filters
from .models import Auto, Moto
from .csv.cars import brand_choice, models_choice



class AutoFilter(django_filters.FilterSet):
    class Meta:
        model = Auto
        fields = (
            'brand',
            'model',
            'type_of_body',
            'transmission',
            'drive_unit',
            'engine_capacity',
            'mileage',
            'year',
            'price',
            )



class MotoFilter(django_filters.FilterSet):
    class Meta:
        model = Moto
        fields = (
            'brand',
            'model',
            'engine_capacity',
            'mileage',
            'year',
            'price',
            )
