from django.shortcuts import render, get_object_or_404, redirect
from .models import *
from .forms import PostAutoForm, PostMotoForm, AutoReviewForm, MotoReviewForm, EmailPostForm, SearchForm
from django.utils import timezone
from .filters import AutoFilter, MotoFilter
# from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import ListView
from django.core.mail import send_mail
#from django.contrib.postgres.search import SearchVector


def auto_share(request, pk):
    car = get_object_or_404(Auto, pk=pk)
    sent = False
    if request.method == 'POST':
        form = EmailPostForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            auto_url = request.build_absolute_uri(car.get_absolute_url())
            subject = f"{cd['name']} Рекомендует взглянуть на эту тачку" \
                      f"{car.brand} - {car.model}"
            message = f"Смотри {car.brand} - {car.model} по ссылке {auto_url}\n\n" \
                      f"Комментарий от {cd['name']}: {cd['comments']}"
            send_mail(subject, message, 'ibahutski@gmail.com',
                      [cd['to']])
            sent = True
    else:
        form = EmailPostForm()
    context = {
        'car': car,
        'form': form,
        'sent': sent,
    }
    return render(request, 'auto_share.html', context)


class AutoListView(ListView):
    queryset = Auto.objects.all().order_by('published_date')[::-1]
    context_object_name = 'cars'
    paginate_by = 2
    template_name = 'auto.html'


# def auto(request):
#     object_list = Auto.objects.all().order_by('published_date')[::-1]
#     paginator = Paginator(object_list, 2)  # 3 posts in each page
#     page = request.GET.get('page')
#     try:
#         cars = paginator.page(page)
#     except PageNotAnInteger:
#     # If page is not an integer deliver the first page
#         cars = paginator.page(1)
#     except EmptyPage:
#         # If page is out of range deliver last page
#         cars = paginator.page(paginator.num_pages)
#     context = {
#         'cars': cars,
#         'page': page
#     }
#     return render(request, 'auto.html', context)


def auto_details(request, pk):
    car = get_object_or_404(Auto, pk=pk)
    queryset = UserAutoMarks.objects.filter(user=request.user, auto=car).first()
    context = {
        'car': car,
        'queryset': queryset,
    }
    return render(request, 'auto_details.html', context)


def delete_auto(request, pk):
    car = get_object_or_404(Auto, pk=pk)
    car.delete()
    return redirect('ab:auto')


def auto_filter(request):
    a_filter = AutoFilter(request.GET, queryset=Auto.objects.all().order_by('published_date').reverse())
    context = {
        'a_filter': a_filter,
    }
    return render(request, 'auto_filters.html', context)


def moto_filter(request):
    m_filter = MotoFilter(request.GET, queryset=Moto.objects.all().order_by('published_date').reverse())
    context = {
        'm_filter': m_filter,
    }
    return render(request, 'moto_filters.html', context)


class MotoListView(ListView):
    queryset = Moto.objects.all().order_by('published_date')[::-1]
    context_object_name = 'motors'
    paginate_by = 2
    template_name = 'moto.html'


# def moto(request):
#     object_list = Moto.objects.all().order_by('published_date')[::-1]
#     paginator = Paginator(object_list, 2)  # 3 posts in each page
#     page = request.GET.get('page')
#     try:
#         motors = paginator.page(page)
#     except PageNotAnInteger:
#     # If page is not an integer deliver the first page
#         motors = paginator.page(1)
#     except EmptyPage:
#         # If page is out of range deliver last page
#         motors = paginator.page(paginator.num_pages)
#     context = {
#         'motors': motors,
#         'page': page
#     }
#     return render(request, 'moto.html', context)


def moto_details(request, pk):
    motor = get_object_or_404(Moto, pk=pk)
    return render(request, 'moto_details.html', {'motor': motor})


def delete_moto(request, pk):
    motor = get_object_or_404(Moto, pk=pk)
    motor.delete()
    return redirect('ab:moto')


def new_ad(request):
    if request.method == "POST":
        form = PostAutoForm(request.POST, request.FILES)
        if form.is_valid():
            car = form.save(commit=False)
            car.author = request.user
            car.published_date = timezone.now()
            form.save()
            return redirect('ab:auto')
    else:
        form = PostAutoForm()
    return render(request, 'new_ad.html', {'form': form})


def new_ad_moto(request):
    if request.method == "POST":
        form = PostMotoForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            form.save()
            return redirect('ab:moto')
    else:
        form = PostMotoForm()
    return render(request, 'new_ad.html', {'form': form})


def auto_bookmarks(request):
    cars = [i.auto for i in UserAutoMarks.objects.filter(user=request.user)]
    return render(request, 'a_bookmarks.html', {'cars': cars})


def auto_bookmark(request, pk):
    user = request.user
    car = get_object_or_404(Auto, pk=pk)
    queryset = UserAutoMarks.objects.filter(user=user, auto=car).first()
    if not queryset:
        UserAutoMarks.objects.create(user=user, auto=car, in_bookmarks=True)
    else:
        UserAutoMarks.objects.get(user=user, auto=car, in_bookmarks=True).delete()

    return redirect('ab:auto_details', pk=car.pk)


def moto_bookmarks(request):
    motors = [i.moto for i in UserMotoMarks.objects.filter(user=request.user)]
    return render(request, 'm_bookmarks.html', {'motors': motors})


def moto_bookmark(request, pk):
    user = request.user
    motor = get_object_or_404(Moto, pk=pk)
    if not UserMotoMarks.objects.filter(user=user, moto=motor).first():
        UserMotoMarks.objects.create(user=user, moto=motor, in_bookmarks=True)

    return render(request, 'moto_details.html', {'motor': motor})


def all_reviews(request):
    all_a_rew = [i for i in AutoReviews.objects.filter(author=request.user)[::-1]]
    all_m_rew = [i for i in MotoReviews.objects.filter(author=request.user)[::-1]]

    context = {
        'all_a_rew': all_a_rew,
        'all_m_rew': all_m_rew
    }
    return render(request, 'all_reviews.html', context)


def auto_create_review(request, pk):
    car = get_object_or_404(Auto, pk=pk)
    if request.method == "POST":
        form = AutoReviewForm(request.POST)
        if form.is_valid():
            rew = form.save(commit=False)
            rew.author = request.user
            rew.reviewed = car
            form.save()
            return redirect('ab:auto_details', pk=car.pk)
    else:
        form = AutoReviewForm()
    return render(request, 'create_review.html', {'form': form})


def a_rev_details(request, pk):
    rew = get_object_or_404(AutoReviews, pk=pk)
    return render(request, 'a_rev_details.html', {'rew': rew})


def a_rev_delete(request, pk):
    rew = get_object_or_404(AutoReviews, pk=pk)
    rew.delete()
    return redirect('ab:all_rew_about_this_car', pk=rew.reviewed.pk)


def all_rew_about_this_car(request, pk):
    pk_auto = get_object_or_404(Auto, pk=pk)
    all_rew_one_car = [i for i in AutoReviews.objects.filter(reviewed_id=pk_auto, author=request.user)[::-1]]
    context = {
        'all_rew_one_car': all_rew_one_car,
        'pk_auto': pk_auto,
    }
    return render(request, 'all_rew_about_car.html', context)


def moto_create_review(request, pk):
    motor = get_object_or_404(Moto, pk=pk)
    if request.method == "POST":
        form = MotoReviewForm(request.POST)
        if form.is_valid():
            rew = form.save(commit=False)
            rew.author = request.user
            rew.reviewed = motor
            form.save()
            return redirect('ab:moto_details', pk=motor.pk)
    else:
        form = MotoReviewForm()
    return render(request, 'create_review.html', {'form': form})


def m_rev_details(request, pk):
    rew = get_object_or_404(MotoReviews, pk=pk)
    return render(request, 'm_rev_details.html', {'rew': rew})


def m_rev_delete(request, pk):
    rew = get_object_or_404(MotoReviews, pk=pk)
    rew.delete()
    return redirect('ab:all_rew_about_this_motor', pk=rew.reviewed.pk)


def all_rew_about_this_motor(request, pk):
    pk_moto = get_object_or_404(Moto, pk=pk)
    all_rew_one_motor = [i for i in MotoReviews.objects.filter(reviewed_id=pk_moto, author=request.user)[::-1]]
    context = {
        'all_rew_one_motor': all_rew_one_motor,
        'pk_moto': pk_moto,
    }
    return render(request, 'all_rew_about_motor.html', context)


def auto_search(request):
    # form = SearchForm()
    # query = None
    # results = []
    # if 'query' in request.GET:
    #     form = SearchForm(request.GET)
    # if form.is_valid():
    #     query = form.cleaned_data['query']
    #     results = Auto.objects.annotate(search=SearchVector('brand', 'model', 'year', 'location',),).filter(search=query)
    # context = {
    #     'form': form,
    #     'query': query,
    #     'results': results}
    # return render(request, 'search.html', context)
    return render (request, 'search.html')
