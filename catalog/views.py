from django.shortcuts import render, get_object_or_404
from cart.forms import CartAddProductForm
from .models import Category, SubCategory, Product
from index.models import Currency


# def category_list(request, category_slug=None, subcategory_slug=None):
#     category = None
#     categories = Category.objects.all()
#     subcategory = None
#     subcategories = SubCategory.objects.all()
#     products = Product.objects.filter(available=True)
#     if category_slug:
#         category = get_object_or_404(Category, slug=category_slug)
#         subcategory = get_object_or_404(SubCategory, slug=subcategory_slug)
#         products = products.filter(category=category)
#     return render(request,
#                   'category.html',
#                   {'category': category,
#                    'categories': categories,
#                    'subcategory': subcategory,
#                    'subcategories': subcategories,
#                    'products': products})
#
def category_list(request, category_slug=None):
    category = None
    categories = Category.objects.all().order_by('sort')
    subcategorys = SubCategory.objects.all()
    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        subcategorys = subcategorys.filter(category=category)
    return render(request, 'category.html',
                  {'category': category, 'categories': categories, 'subcategorys':subcategorys})


def subcategory_list(request, category_slug=None):
    category = None
    categories = Category.objects.all().order_by('sort')
    subcategorys = SubCategory.objects.all()
    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        subcategorys = subcategorys.filter(category=category)
    return render(request, 'subcategory.html',
                  {'category': category, 'categories': categories,
                   'subcategorys': subcategorys})


def product_list(request, subcategory_slug=None, category_slug=None):
    subcategory = None
    subcategories = SubCategory.objects.all()
    products = Product.objects.filter(available=True)
    if subcategory_slug:
        category = get_object_or_404(Category, slug=category_slug)
        subcategory = get_object_or_404(SubCategory, slug=subcategory_slug)
        products = products.filter(subcategory=subcategory)
    return render(request, 'product_list.html',
                  {'category': category, 'subcategory': subcategory, 'subcategories': subcategories,
                   'products': products, })


def product_detail(request, slug, category_slug=None, subcategory_slug=None):
    product = get_object_or_404(Product,
                                slug=slug,
                                available=True)
    # Add to cart button
    cart_product_form = CartAddProductForm()
    return render(request,
                  'product_detail.html',
                  {'product': product,
                   'cart_product_form': cart_product_form
                   })
