from django.conf.urls import url
from . import views
from django.urls import path
from .views import category_list, subcategory_list, product_list, product_detail

namespace = 'catalog'
appname = 'catalog'

# urlpatterns = [
#     url(r'^$', views.category_list, name='category_list'),
#     url(r'^(?P<category_slug>[-\w]+)/$', views.subcategory_list, name='subcategory_list'),
#     url(r'^(?P<category_slug>[-\w]+)/(?P<subcategory_slug>[-\w]+)/$', views.product_list, name='product_list'),
#     url(r'^(?P<id>\d+)/$', views.product_detail,name='product_detail'),
# ]
urlpatterns = [
    path('', views.category_list, name='category_list'),
    path('<slug:category_slug>/', views.subcategory_list, name='subcategory_list'),
    path('<slug:category_slug>/<slug:subcategory_slug>/', views.product_list, name='product_list'),
    path('<slug:category_slug>/<slug:subcategory_slug>/<slug:slug>/', views.product_detail,
         name='product_detail'),

]

# urlpatterns = [
#     url(r'^', views.category_list, name='category_list'),
#     url(r'^(?P<category_slug>[-\w]+)/$', views.subcategory_list, name='subcategory_list'),
#     url(r'^(?P<subcategory_slug>[-\w]+)/$', views.product_list, name='product_list'),
#     url(r'^(?P<id>\d+)/(?P<slug>[-\w]+)/$', views.product_detail, name='product_detail'),
# ]
