from django.contrib import admin
from .models import Category, SubCategory, Product


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'sort']
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(Category, CategoryAdmin)


class SubCategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'sort']
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(SubCategory, SubCategoryAdmin)


class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'price', 'stock', 'available', 'created',
                    'updated',]
    list_filter = ['available', 'created', 'updated']
    list_editable = ['price', 'stock', 'available']
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(Product, ProductAdmin)


# class ProperyTypeAdmin(admin.ModelAdmin):
#     list_display = ['name', 'type']
#
#
# admin.site.register(ProperyType, ProperyTypeAdmin)
#
#
#
# class PropertyInstanceAdmin(admin.ModelAdmin):
#     list_display = ['name', 'type', 'value']
#
#
# admin.site.register(PropertyInstance, PropertyInstanceAdmin)
