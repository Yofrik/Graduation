# Generated by Django 3.2.9 on 2022-01-04 15:18

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('index', '0003_alter_currency_cur_officialrate'),
    ]

    operations = [
        migrations.RenameField(
            model_name='currency',
            old_name='Cut_Name',
            new_name='Cur_Name',
        ),
    ]
