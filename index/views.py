from django.shortcuts import render, get_object_or_404
from auto.models import Auto
from .models import Currency
from news.models import Post, Category
from catalog.models import Product
from urllib import request
import requests
import json
from django.http import HttpResponse, HttpResponseNotFound
from urllib.request import urlopen
from django.db.models import Subquery
from.parser import CoronaStat


def index(request, slug=None):
    categories = Category.objects.all()
    posts = Post.objects.order_by('published')
    five_auto = Auto.objects.all().order_by('published_date')[::-1][:5]
    stat = CoronaStat()
    url = 'http://api.openweathermap.org/data/2.5/weather?q={}&units=metric&appid=fabe2b7dd8d35d8ad2d80d7445b85fb9'
    city = 'Minsk'
    city_weather = requests.get(
        url.format(city)).json()  # request the API data and convert the JSON to Python data types
    weather = {
        'city': city,
        'temperature': city_weather['main']['temp'],
        'description': city_weather['weather'][0]['description'],
        'icon': city_weather['weather'][0]['icon']
    }

    context = {
        'categories': categories,
        'posts': posts,
        'five_auto': five_auto,
        'weather': weather,
        'stat': stat,
    }

    return render(request, 'index.html', context)


def currenc(request):
    currens = Currency.objects.all()
    context = {
        'currens': currens,
    }
    return render(request, 'currency.html', context)
