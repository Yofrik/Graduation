from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf.urls import url
from django.conf import settings
from . import settings
# from news.views import index, home_view
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    path('', include('index.urls')),
    path('', include("news.urls")),
    url(r'^admin/', admin.site.urls),
    url(r'^cart/', include(('cart.urls', 'cart'), namespace='cart')),
    url(r'orders/', include(('orders.urls', 'orders'), namespace='orders')),
    url(r'^coupons/', include(('coupons.urls', 'coupons'), namespace='coupons')),
    url(r'^catalog/', include(('catalog.urls', 'catalog'), namespace='catalog')),
    url(r'^news/', include(('news.urls', 'news'), namespace='news')),
    url(r'^ab/', include(('auto.urls', 'ab'), namespace='ab')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('payment/', include('payment.urls', namespace='payment')),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
