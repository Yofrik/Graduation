from catalog.models import Product
from index.models import Currency
from urllib import request
import requests
from datetime import datetime
import pytz

OurTZ = pytz.timezone('Europe/Minsk')

def currenc():
    currens = Currency.objects.all()
    url = "https://www.nbrb.by/api/exrates/rates?periodicity=0"
    response = requests.get(url).json()
    for currencys in response:
        if currens:
            Currency.objects.filter(Cur_Abbreviation=currencys['Cur_Abbreviation']).update(
                Cur_Scale=currencys['Cur_Scale'],
                Cur_OfficialRate=currencys['Cur_OfficialRate'], date=datetime.now(OurTZ))

        else:
            Currency.objects.create(Cur_Name=currencys['Cur_Name'], Cur_Abbreviation=currencys['Cur_Abbreviation'],
                                    Cur_Scale=currencys['Cur_Scale'], Cur_OfficialRate=currencys['Cur_OfficialRate'],
                                    date=datetime.now(OurTZ)
                                    )

    currens_USD = Currency.objects.filter(Cur_Abbreviation="USD")
    for i in currens_USD:
        Value_Price_USD = i.Cur_OfficialRate
    products = Product.objects.all()
    for product in products:
        Value_Price = int(product.price)
        Exchange_price = Value_Price / Value_Price_USD
        product.price_usd = Exchange_price
        product.save()
    context = {
        'currens': currens,
    }
    return context
